

const express = require('express');
const app = express();
const puerto = 3004;

const telefonos = [
    {
        marca: 'Apple',
        gama: 'alta',
        modelo: 'iPhone 6',
        precio: 1299,
        pantalla: '6',
        sistema_operativo: 'iOS'
    },
    {
        marca: 'Xiaomi',
        gama: 'media',
        modelo: 'Redmi Note 3',
        precio: 399,
        pantalla: '4',
        sistema_operativo: 'Android'
    },
    {
        marca: 'Realme',
        gama: 'media',
        modelo: '7',
        precio: 244,
        pantalla: '5',
        sistema_operativo: 'Android'
    },
    {
        marca: 'Sony Ericsson',
        gama: 'alta',
        modelo: 'k850',
        precio: 544,
        pantalla: '5',
        sistema_operativo: 'Java'
    },
    {
        marca: 'Lg',
        gama: 'baja',
        modelo: 'k22',
        precio: 200,
        pantalla: '6',
        sistema_operativo: 'Android'
    },
    {
        marca: 'Samsung',
        gama: 'alta',
        modelo: 'Galaxy S7',
        precio: 1299,
        pantalla: '6',
        sistema_operativo: 'Android'
    }];

app.get('/telefonos', (req, res) => {
    res.json(telefonos);
});

app.get('/gamaAlta', (req, res) => {
    res.json(telefonos.filter(telefono => telefono.gama === 'alta'));
});

app.get('/mitad', (req, res) => {
    res.json(telefonos.slice(0, telefonos.length / 2));

});


app.get('/menorPrecio', (req, res) => {
    let menPrecio = [];
    let mPrecio = telefonos.sort((a, b) => a.precio - b.precio);
    for (let i = 0; i < mPrecio.length; i++) {
        if (mPrecio[0].precio === mPrecio[i].precio) {
            menPrecio.push(mPrecio[i]);
        }
    }
    res.json(menPrecio);
});

app.get('/mayorPrecio', (req, res) => {
    let mayPrecio = [];
    let mPrecio = telefonos.sort((a, b) => b.precio - a.precio);
    for (let i = 0; i < mPrecio.length; i++) {
        if (mPrecio[0].precio === mPrecio[i].precio) {
            mayPrecio.push(mPrecio[i]);
        }
    }
    res.json(mayPrecio);
});

app.get('/gamas', (req, res) => {
    //res.json(telefonos.filter(telefono => telefono.gama === 'alta'));
    let gamas = []; 
    gamas.push(telefonos.filter(telefono => telefono.gama === 'alta'));
    gamas.push(telefonos.filter(telefono => telefono.gama === 'media'));
    gamas.push(telefonos.filter(telefono => telefono.gama === 'baja'));
    res.json(gamas);
    
});


app.listen(puerto, () => {
    console.log(`Servidor corriendo en http://192.168.200.10:${puerto}`);
});
