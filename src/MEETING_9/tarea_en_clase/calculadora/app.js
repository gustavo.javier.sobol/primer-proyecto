/*
comando para ejecutar hay que pasar dos parametros que sean numeros 
para realizar todas las operaciones 
 node app.js 15 19
*/

const calculadora = require('./calculadora');

let myArgs = process.argv.slice(2);

calculadora.suma(myArgs[0], myArgs[1]);
calculadora.restar(myArgs[0], myArgs[1]);
calculadora.multiplicar(myArgs[0], myArgs[1]);
calculadora.dividir(myArgs[0], myArgs[1]);

