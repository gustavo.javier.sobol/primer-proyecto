const log = require("./logs");

function suma (valorA , valorB){
    let sumaResul = parseInt(valorA, 10) + parseInt(valorB,10);
    let print =`El Resultado de la suma ${valorA} + ${valorB} es: ${sumaResul}`;
    console.log (print) ;
    log.registrarLog(print);
}

function restar (valorA , valorB){
    let restaResul =  parseInt(valorA, 10) - parseInt(valorB,10);
    let print = `El Resultado de la resta ${valorA} - ${valorB} es: ${restaResul}`;
    console.log (print) ;
    log.registrarLog(print);
}

function multiplicar (valorA , valorB){
    let multiplicarResul = parseInt(valorA, 10) * parseInt(valorB,10);
    let print = `El Resultado de la multiplicacion ${valorA} * ${valorB} es: ${multiplicarResul}`;
    console.log (print);
    log.registrarLog(print);
}

function dividir (valorA , valorB){
    let dividirResul = parseInt(valorA, 10) / parseInt(valorB,10);
    let print = `El Resultado de la división ${valorA} / ${valorB} es: ${dividirResul}`;
    console.log (print) ;
    log.registrarLog(print);
}
exports.suma = suma;
exports.restar = restar;
exports.multiplicar = multiplicar;
exports.dividir = dividir;