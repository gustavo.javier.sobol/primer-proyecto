const moment = require('moment');
const chalk = require('chalk');

//Descomentar para para probar la comparacion de dias con add le sumo un dia mas
current = moment();//.add(1,'days');
currentUTC = moment.utc();
//Imprimo los horarios
console.log(chalk.bgGreen(`Horario Local: ${current}`));
console.log(chalk.bgGray(`Horario UTC: ${currentUTC}`)); 
//Convierto fecha toArray que me sirve para poder hacer las diff con moment
const dateCurrent = moment(current.toArray());
const dateCurrentUTC = moment(currentUTC.toArray());
// Obtengo la diferencia Horaria
const result = dateCurrent.diff(dateCurrentUTC, 'hours'); 
//Imprimo la diferencia horaria
console.log(chalk.bgMagenta(`La Diferencia Horaria es de  ${result}`));
// Realizo la comparacion e imprimo 
result < 0 ? console.log(`estoy detrás del UTC`) : console.log(`estoy por delante del UTC`);
console.log(chalk.blue(`con una diferencia de ${Math.abs(result)} horas`));
//Tuve que convertirlo con format por que o sino daba error a veces 
moment(current.format("YYYY-MM-DD HH:mm")).isSameOrAfter(currentUTC.format("YYYY-MM-DD HH:mm")) ? console.log(`Actual ${current} es mayor` ) : console.log(`UTC ${currentUTC} es mayor`);


